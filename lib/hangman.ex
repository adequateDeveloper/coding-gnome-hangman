defmodule Hangman do
  @moduledoc ~S"""
  The Hangman game public API.

  ## Examples

      iex> game_pid = Hangman.new_game
      iex> game_state = Hangman.make_move game_pid, "a"
      iex> report = Hangman.tally game_pid
  """

  alias Hangman.Server

  @doc """
  Initialize a new game.
  """
  @spec new_game :: pid
  defdelegate new_game, to: Server

  @type game_state() :: %Hangman.Game{
          game_state: atom,
          letters: list[String.t()],
          turns_left: integer,
          used: list[String.t()]
        }

  @doc """
  Make a game move and return the current state of the game
  """
  @spec make_move(pid, String.t()) :: game_state()
  defdelegate make_move(game_pid, guess), to: Server

  @type tally_result() :: %{
          game_state: atom,
          letters: list[String.t()],
          turns_left: integer,
          used: list[String.t()],
          word: list[String.t()]
        }

  @doc """
  Get the game tally report
  """
  @spec tally(pid) :: tally_result()
  defdelegate tally(game_pid), to: Server
end
