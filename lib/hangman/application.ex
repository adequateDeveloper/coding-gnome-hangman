defmodule Hangman.Application do
  @moduledoc false

  # Conversion to DynamicSupervisor - see video:
  # https://pragdave.me/blog/2018/01/28/dynamic-supervisors.html

  use Application

  def start(_type, _args) do
    # Because of the DynamicSupervisor :one_for_one strategy, a Server instance is not started when its Supervisor is started.
    # Instead, the Server instance is initiated from a public API call to Hangman.new_game/0, which delegates to
    # Server.new_game/0, which instructs the DynamicSupervisor to start an instance of the child.
    # The coupling between Hangman.Application and Hangman.Server is the supervisor name, Hangman.Supervisor.

    options = [
      name: Hangman.Supervisor,
      strategy: :one_for_one
    ]

    DynamicSupervisor.start_link(options)
  end
end
