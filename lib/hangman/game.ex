defmodule Hangman.Game do
  @moduledoc """
  Implementation of the Hangman Game.
  """

  # TODO:
  # change from embedded states & state transitions to a rules engine

  # Game states:
  # :initializing, :good_guess, :have_used, :bad_guess, :won, :lost

  alias Hangman.Game

  @compile if Mix.env() == :test, do: :export_all

  defstruct(
    turns_left: 7,
    game_state: :initializing,
    letters: [],
    used: MapSet.new()
  )

  @doc """
  Start a new Hangman game.
  """
  @spec new_game :: %Game{}
  def new_game do
    %Game{letters: Dictionary.random_word() |> String.codepoints()}
  end

  @doc """
  Process the game guess.
  """
  # if the game state is already :won or :lost then return the unchanged game
  @spec make_move(%Game{}, binary) :: %Game{}
  def make_move(game = %Game{game_state: current_state}, _guess)
      when current_state in [:won, :lost] do
    game
  end

  # otherwise, process the guess and update the game state
  def make_move(game, guess) do
    accept_move(game, guess, MapSet.member?(game.used, guess))
  end

  @type game_tally() :: %{
          game_state: atom,
          letters: list[binary],
          turns_left: integer,
          used: list[binary],
          word: list[binary]
        }

  @doc """
  Get the current state of the game
  """
  @spec tally(%Game{}) :: game_tally
  def tally(game) do
    %{
      game_state: game.game_state,
      turns_left: game.turns_left,
      used: game.used |> MapSet.to_list() |> Enum.sort(),
      letters: game.letters |> reveal_guessed(game.used),
      word: game.letters
    }
  end

  # if the guess has been used already, set the game state and return
  defp accept_move(game, _guess, _have_guessed = true) do
    %{game | game_state: :have_used}
  end

  # otherwise, process the new guess and update the game
  defp accept_move(game, guess, _have_guessed) do
    %{game | used: MapSet.put(game.used, guess)}
    |> score_guess(Enum.member?(game.letters, guess))
  end

  # the latest game word letter guess is correct. Update the game state.
  defp score_guess(game, _guessed = true) do
    new_game_state =
      MapSet.new(game.letters)
      |> MapSet.subset?(game.used)
      |> maybe_won()

    game = %{game | game_state: new_game_state}

    # TODO:
    # if new_game_state == :won, do: save_game_result("WON", game)
    # update tally with # of won & lost games for the session

    game
  end

  # bad letter guess and last try. Update the game state to :lost
  defp score_guess(game = %Game{turns_left: 1}, _guessed) do
    game = %{game | game_state: :lost, turns_left: 0}

    # TODO:
    # save_game_result("LOST", game)
    # update tally with # of won & lost games for the session

    game
  end

  # bad letter guess but there are remaining trys. Update the game state to :bad_guess
  defp score_guess(game = %Game{turns_left: turns_left}, _guessed) do
    %{game | game_state: :bad_guess, turns_left: turns_left - 1}
  end

  # if the set of game word letters are in the set of used letters,
  # then the game is in won state. Otherwise, the game is in :good_guess state.
  defp maybe_won(true), do: :won
  defp maybe_won(_false), do: :good_guess

  # reveal which letters of the game word have been guessed
  defp reveal_guessed(letters, used) do
    letters
    # |> Enum.map(fn letter -> reveal_letter(letter, MapSet.member?(used, letter)) end)
    |> Enum.map(&reveal_letter(&1, MapSet.member?(used, &1)))
  end

  # reveal the game word letter if it has been guessed, otherwise return a "-"
  defp reveal_letter(letter, _in_word = true), do: letter
  defp reveal_letter(_letter, _not_in_word), do: "-"

  # TODO:
  # persist the results of the latest game for the playing session
  # defp save_game_result(state, game) do
  #    Storage.save_game_result(state, Enum.join(game.letters), Enum.count(game.used, fn(x) -> x end))
  # end
end
