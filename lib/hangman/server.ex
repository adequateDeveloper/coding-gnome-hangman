defmodule Hangman.Server do
  @moduledoc false

  #  Design Approach
  #  ---------------
  #  Ref: https://pragdave.me/blog/2017/07/13/decoupling-interface-and-implementation-in-elixir.html

  #  This server is invoked from the public API via Hangman.new_game/0 and consequently delegates game processing to the
  #  Game implementation via GenServer callbacks.

  #  The way I split the public API (Hangman), the server (Server), and the implementation (Game) is not the normal way people
  #  code in Elixir. At the end of this chapter I have a unit showing how most people would simply put all three into a
  #  single file.

  #  I did the same until recently. But doing so lead to some big, kitchen sink, modules. It also means that developing
  #  your code is an all-or-nothing thing: your implementation is spread across a number of callbacks that are also
  #  responsible for maintaining the server state.

  #  To my mind, this approach means that a single module has three separate concerns: API, server, and implementation.

  #  So now I split the code into three modules. The API always sits at the top level, with the implementation and server
  #  in the subdirectory. The server is solely responsible for handling the GenServer callbacks and managing the state.
  #  Finally, the implementation is just a standalone set of functions that know how to get some problem solved, but that
  #  knows nothing about the client, and nothing about being a server. The implementation module is still usable as
  #  simple, standalone code.

  #  See also: Supervisor notes in application.ex

  use GenServer

  alias Hangman.Game

  #  Examples:

  #  at Server level:
  #  {:ok, pid} = Hangman.Server.start_link
  #  GenServer.call(pid, {:make_move, "a"})
  #  GenServer.call(pid, {:tally})

  #  at Hangman public API level:
  #  pid = Hangman.new_game
  #  Hangman.make_move pid, "a"
  #  Hangman.tally pid

  # delegated calls

  def new_game do
    {:ok, pid} = DynamicSupervisor.start_child(Hangman.Supervisor, __MODULE__)
    pid
  end

  def make_move(game_pid, guess), do: GenServer.call(game_pid, {:make_move, guess})

  def tally(game_pid), do: GenServer.call(game_pid, {:tally})

  # callbacks

  def start_link(_), do: GenServer.start_link(__MODULE__, nil)

  @impl true
  def init(_args), do: {:ok, Game.new_game()}

  @impl true
  def handle_call({:make_move, guess}, _from, game_state) do
    updated_game = Game.make_move(game_state, guess)
    {:reply, updated_game, updated_game}
  end

  @impl true
  def handle_call({:tally}, _from, game_state), do: {:reply, Game.tally(game_state), game_state}
end
