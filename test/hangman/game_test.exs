defmodule GameTest do
  use ExUnit.Case, async: true
  alias Hangman.Game
  #  doctest Game

  setup do
    {:ok, new_game: Game.new_game()}
  end

  describe "new_game/0" do
    test "returns correct struct", %{new_game: game} do
      expected_keys = [:__struct__, :game_state, :letters, :turns_left, :used]

      actual_keys = Map.keys(game)

      assert actual_keys == expected_keys
      # assert Enum.all?(game.letters, fn it -> is_binary(it) end)
      assert Enum.all?(game.letters, &is_binary(&1))
    end

    test "returns default game state", %{new_game: game} do
      assert game.turns_left == 7
      assert game.game_state == :initializing
      assert is_list(game.letters)
      assert length(game.letters) > 0
      assert is_map(game.used)
      assert MapSet.size(game.used) == 0
    end
  end

  describe "tally/1" do
    test "returns correct map on initialization", %{new_game: game} do
      expected_keys = [:game_state, :letters, :turns_left, :used, :word]

      tally_result = Game.tally(game)
      actual_keys = Map.keys(tally_result)

      assert actual_keys == expected_keys
      assert :initializing == tally_result.game_state
      assert 7 == tally_result.turns_left
      assert [] == tally_result.used
    end
  end

  describe "make_move/2" do
    test "with game_state: of :won or :lost returns unchanged game state", %{
      new_game: game
    } do
      for next_state <- [:won, :lost] do
        expected_game = %{game | game_state: next_state}

        assert ^expected_game = Game.make_move(expected_game, "x")
      end
    end

    test "with game_state other than :won or :lost updates game state", %{
      new_game: game
    } do
      first_letter = List.first(game.letters)

      game = Game.make_move(game, first_letter)

      assert game.turns_left == 7
      assert game.game_state == :good_guess
      assert [first_letter] == MapSet.to_list(game.used)
    end

    test "with wrong guesses results in game_state of :lost", %{new_game: game} do
      wrong_letters = ["1", "2", "3", "4", "5", "6"]
      new_used = for wrong_letter <- wrong_letters, into: game.used, do: wrong_letter

      # game = %{game | game_state: :bad_guess, turns_left: 1, used: MapSet.new(wrong_letters)}
      game = %{game | game_state: :bad_guess, turns_left: 1, used: new_used}
      game = Game.make_move(game, "7")

      assert game.turns_left == 0
      assert game.game_state == :lost
    end
  end

  describe "reveal_letter/2" do
    test "when letter in word returns letter" do
      in_word = true
      assert "x" == Game.reveal_letter("x", in_word)
    end

    test "when letter not in word returns '-'" do
      in_word = false
      assert "-" == Game.reveal_letter("x", in_word)
    end
  end

  describe "reveal_guessed/2" do
    test "when letters == used returns all letters" do
      letters = ["a", "b", "c"]
      used = MapSet.new(letters, fn letter -> letter end)
      assert ["a", "b", "c"] == Game.reveal_guessed(letters, used)
    end

    test "when letters != used returns both letters and '-'" do
      letters = ["a", "b", "c"]
      used = MapSet.new(["a", "x", "c"], fn letter -> letter end)
      assert ["a", "-", "c"] == Game.reveal_guessed(letters, used)
    end

    test "when letters != used returns only '-'" do
      letters = ["a", "b", "c"]
      used = MapSet.new(["x", "y", "z"], fn letter -> letter end)
      assert ["-", "-", "-"] == Game.reveal_guessed(letters, used)
    end
  end

  test "maybe_won/1 returns correct game_state" do
    assert :won == Game.maybe_won(true)
    assert :good_guess == Game.maybe_won(false)
  end

  describe "score_guess/2" do
    test "with bad guess and 1 remaining turn results in lost game", %{new_game: game} do
      guessed = false

      game = %{game | turns_left: 1}
      game = Game.score_guess(game, guessed)

      assert game.turns_left == 0
      assert game.game_state == :lost
    end

    test "with bad guess and more than 1 remaining turn updates game_state to :bad_guess", %{
      new_game: game
    } do
      guessed = false

      game = %{game | turns_left: 2}
      game = Game.score_guess(game, guessed)

      assert game.turns_left == 1
      assert game.game_state == :bad_guess
    end

    test "with a single letter correct guess updates game_state to :good_guess", %{new_game: game} do
      first_letter = List.first(game.letters)
      game = %{game | used: MapSet.put(game.used, first_letter)}
      guessed = true

      game = Game.score_guess(game, guessed)

      assert game.turns_left == 7
      assert game.game_state == :good_guess
    end

    test "with all letters guessed updates game_state to :won", %{new_game: game} do
      new_used = for letter <- game.letters, into: game.used, do: letter
      game = %{game | used: new_used}
      guessed = true

      game = Game.score_guess(game, guessed)

      assert game.turns_left == 7
      assert game.game_state == :won
    end
  end

  describe "accept_move/3" do
    test "with repeating guess updates game_state: to :have_used", %{new_game: game} do
      a_guess = "x"
      have_guessed = true

      refute game.game_state == :have_used

      game = Game.accept_move(game, a_guess, have_guessed)

      assert game.game_state == :have_used
    end

    test "with new guess updates game used: and not game_state:", %{new_game: game} do
      a_guess = "x"
      have_guessed = false

      refute game.game_state == :have_used
      refute MapSet.member?(game.used, a_guess)

      game = Game.accept_move(game, a_guess, have_guessed)

      assert MapSet.member?(game.used, a_guess)
      refute game.game_state == :have_used
    end
  end
end
