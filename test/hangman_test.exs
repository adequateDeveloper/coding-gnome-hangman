defmodule HangmanTest do
  use ExUnit.Case, async: true
  #  doctest Hangman

  setup do
    {:ok, new_game_pid: Hangman.new_game()}
  end

  test "new_game/0 returns pid", %{new_game_pid: game_pid} do
    assert is_pid(game_pid)
  end

  describe "make_move/2" do
    test "returns correct structure", %{new_game_pid: game_pid} do
      game = Hangman.make_move(game_pid, "a")

      expected_keys = [:__struct__, :game_state, :letters, :turns_left, :used]

      actual_keys = Map.keys(game)
      assert actual_keys == expected_keys
    end

    test "returns one of the allowed game states", %{new_game_pid: game_pid} do
      game_states = [:initializing, :good_guess, :bad_guess, :have_used, :won, :lost]

      game = Hangman.make_move(game_pid, "a")

      assert Enumerable.member?(game_states, game.game_state)
    end

    test "returns default game struct values", %{new_game_pid: game_pid} do
      game = Hangman.make_move(game_pid, "a")

      assert game.turns_left >= 6
      assert is_atom(game.game_state)
      assert is_map(game.used)
      assert is_list(game.letters)
      assert length(game.letters) > 0
      assert Enum.all?(game.letters, fn it -> is_binary(it) end)
    end
  end

  describe "tally/1" do
    test "returns correct map", %{new_game_pid: game_pid} do
      expected_keys = [:game_state, :letters, :turns_left, :used, :word]

      result = Hangman.tally(game_pid)
      actual_keys = Map.keys(result)

      assert actual_keys == expected_keys
    end

    test "returns correct initial score", %{new_game_pid: game_pid} do
      result = Hangman.tally(game_pid)

      assert result.game_state == :initializing
      assert result.turns_left == 7
    end
  end
end
