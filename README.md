# Hangman
[![pipeline status](https://gitlab.com/adequateDeveloper/coding-gnome-hangman/badges/master/pipeline.svg)](https://gitlab.com/adequateDeveloper/coding-gnome-hangman/commits/master)

[Following the Dave Thomas Elixir for Programmers course](https://coding-gnome.thinkific.com/courses/elixir-for-programmers)

## Generating Project Documentation

From the project root folder run the following two mix commands:

```elixir
$ mix deps.get # gets ExDoc.
$ mix docs # makes the documentation.

Docs successfully generated.
View them at "doc/index.html".
```

## Features

* Updated to Elixir v1.7
* Dictionary component dependency pulled from GitHub
* Top level public API that delegates implementation
* Uses DynamicSupervisor for dispatching new games
* Exports private functions for testing
* Uses mix format (see: .formatter.exs)
* ExDoc package for project documentation
* ExCoveralls package for test coverage statistics
* Credo package for static code analysis
* Dialyxir package for static API analysis
* InchEx package for measuring documentation coverage
* Mix alias - quality - runs credo, inch, & dialyzer
